# chrotran-cr-bio for ["Pajarito Models"](https://gitlab.com/lanl-em)

chrotran-cr-bio is a ["Pajarito Models"](https://gitlab.com/lanl-em) module.

All modules under ["Pajarito Models"](https://gitlab.com/lanl-em) are open-source released under GNU GENERAL PUBLIC LICENSE Version 3.