import DataStructures

using LaTeXStrings

coolnames  = DataStructures.OrderedDict{AbstractString,LaTeXStrings.LaTeXString}(
"log_MASS_ACTION_CD"=>L"\mathrm{\Gamma_{CD}}",
"log_RATE_B_1"=>L"\mathrm{\lambda_{B_1}}",
"log_RATE_B_2"=>L"\mathrm{\lambda_{B_2}}",
"log_RATE_C"=>L"\mathrm{\lambda_{C}}",
"log_INHIBITION_B"=>L"\mathrm{K_B}",
"log_INHIBITION_C"=>L"\mathrm{K_C}",
"log_MONOD_D"=>L"\mathrm{K_D}",
"log_INHIBITION_I"=>L"\mathrm{K_I}")
